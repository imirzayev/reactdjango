from django.shortcuts import render
from .models import Lead
from rest_framework import viewsets, permissions
from .serializers import LeadSerializer


# Create your views here.
class LeadViewSet(viewsets.ModelViewSet):
    queryset = Lead.objects.all()
    serializer_class = LeadSerializer
    permission_classes = [
        permissions.AllowAny
    ]
